import firebase from 'firebase';

export default {
  namespaced: true,
  state: {
    currentUser: null,
  },
  mutations: {
    setCurrentUser(state) {
      state.currentUser = firebase.auth().currentUser;
    },
  },
  actions: {
    setCurrentUser({ commit }) {
      commit('setCurrentUser');
    },
  },
  getters: {
    getCurrentUser(state) {
      return state.currentUser;
    },
  },
};
